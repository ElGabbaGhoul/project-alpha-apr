from django.contrib import admin
from projects.models import Project
#from django.contrib import admin
from tasks.models import Task

# error: module 'django.contrib.admin' has no attribute 'ProjectAdmin'
# fix:  needed to put ModelAdmin and not whatever else was there
class ProjectAdmin(admin.ModelAdmin):
    pass


# class CreateAdmin(admin.CreateAdmin):
#     pass

admin.site.register(Project, ProjectAdmin)
admin.site.register(Task)
