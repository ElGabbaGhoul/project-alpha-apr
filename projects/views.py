# from msilib.schema import ListView
#from asyncio import tasks
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView


# from django.shortcuts import render
from projects.models import Project


class ProjectListView(ListView):
    model = Project
    template_name = "projects/list.html"


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/details.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]
    success_url = reverse_lazy("projects_list")
